/* file:  powerful.c           */
/* author:  elias_1233@hotmail.com    */
/* date:  19-09-2016          */
/* version: 1.0             */
/* Description: This program give the number that is equal to the sum of it own numbers to the power of ammount of digits*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

long long int swapDigits(long long int a, long long int b, long long int num){
  long long int temp,reverse,reverseLast, tempnum, reva, revb, newnum, digits, tempdigits;
  tempnum = reva = revb = newnum = 0;
  temp = 0;
  digits = 0;
  digits = log10(num);
  tempdigits = digits;
  reverse = num;
  reverseLast = 0;
  

  while(reverse > 0){
    reverseLast = reverse%10;
    temp = 10*temp + reverseLast;
    reverse = reverse / 10;
  }


  while(temp > 0){
     tempnum = temp % 10;
    if(a == digits){
      reva = tempnum;
    }
    if(b == digits){
      revb = tempnum;
    }
    temp = temp/10;
    digits--;
  }

  long long int power = 1;
  while(num > 0){
    long long int tempnewnum;
    tempnewnum = num % 10;
   
    if(tempnewnum == reva){
      newnum = newnum + revb*power;
    }
    else if(tempnewnum == revb){
      newnum = newnum + reva*power;
    }
     else if((tempnewnum == reva) && (tempnewnum == revb)){
      newnum = newnum + reva*power;
      
    }
    else if((tempnewnum != reva) && (tempnewnum != revb)){
      newnum = newnum + tempnewnum*power;
    }

    power = power * 10;
    num = num/10;
  }
return newnum;
}

int numberOfDigits(long long int x){
  long long int numDigits = 0;
  numDigits = log10(x);
  return numDigits;
}

long long int reverseDigits(long long int x){
  long long int swapped, Digits;
  Digits = numberOfDigits(x);
  swapped = swapDigits(0, Digits, x);
  for(int i = 1; i <= Digits/2; i++){
    swapped = swapDigits(i, Digits - i, swapped);
  }

    return swapped;
  }
  
int main(int argc, char *argv[]){
  long long int x, palindroom;
  scanf("%lli", &x);
  palindroom = reverseDigits(x);
  if(x == 0){
    printf("%lli is not a palindrome.\n", x);
  }
  else if(palindroom == x){
    printf("%lli is a palindrome.\n", x);
  }
  else{
    printf("%lli is not a palindrome.\n", x);
  }
  return 0;
}
