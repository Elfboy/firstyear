/* file:  powerful.c           */
/* author:  elias_1233@hotmail.com    */
/* date:  19-09-2016          */
/* version: 1.0             */
/* Description: This program give the number that is equal to the sum of it own numbers to the power of ammount of digits*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int main(int argc, char *argv[]){

char a = '\0';
char b = '\0';
char c = '\0';

		int R1, R2;
  	R1 = R2 = 0;
	
  	int x, d, e, n= 0;
  while((n = scanf("%c%c%c R%d", &a,&b,&c,&x)) == 4 ){

    if(a == 'L'){
      scanf("%d", &d);
      if(x == 1) {
      	R1 = d;
      } else {
      	R2 = d;	
      }
    }
      
    if(a == 'I'){
      if(x == 1) {
      	R1++;
      } else {
      	R2++;	
      }
    }    

    if(a == 'D'){
    	if(x == 1){
      		R1--;
    	}
    	else{
      		R2--;
    	}
	}

	if(a == 'A'){
		scanf("	R%d", &e);
		if(x == 1){
			if(e == 1){
				R1 = R1 + R1;
			}
			else{
				R2 = R1 + R2;
			}
		}
		else{
			if(e == 1){
				R1 = R1 + R2;
			}
			else{
				R2 = R2 + R2;
			}
		}
	}

	if(a == 'S'){
		scanf("	R%d", &e);
		if(x == 1){
			if(e == 1){
				R1 = R1 - R1;
			}
			else{
				R2 = R1 - R2;
			}
		}
		else{
			if(e == 1){
				R1 = R2 - R1;
			}
			else{
				R2 = R2 - R2;
			}
		}
	}

	if(a == 'M'){
		scanf("	R%d", &e);
		if(x == 1){
			if(e == 1){
				R1 = R1 * R1;
			}
			else{
				R2 = R1 * R2;
			}
		}
		else{
			if(e == 1){
				R1 = R1 * R2;
			}
			else{
				R2 = R2 * R2;
			}
		}
	}

	if(a == 'O'){
		if(x == 1){
			printf("%d\n", R1);
		}
		else{
			printf("%d\n", R2);
		}
	}

    getchar();
  }
  return 0;
}

