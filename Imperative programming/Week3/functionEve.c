/* file:  powerful.c           */
/* author:  elias_1233@hotmail.com    */
/* date:  19-09-2016          */
/* version: 1.0             */
/* Description: This program give the number that is equal to the sum of it own numbers to the power of ammount of digits*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int functionEve(int num){
  int c, sum = 0;
  sum = num;
  while((c = getchar()) != '\n' && c != EOF) {
   if(c == 'f'){
      sum = (sum*sum);
    }
     if(c == 'g'){
      sum += 1;
    }
     if(c == 'h'){
      sum -= 1;
    }
     if(c == '='){
      sum += 0;
   }
}
return sum;
}

int main(int argc, char *argv[]){
  int num, answer;
  scanf("%d", &num);
  answer = functionEve(num);
  printf("%d\n", answer);
return 0;
}
