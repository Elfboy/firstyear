/* file:  powerful.c           */
/* author:  elias_1233@hotmail.com    */
/* date:  19-09-2016          */
/* version: 1.0             */
/* Description: This program give the number that is equal to the sum of it own numbers to the power of ammount of digits*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


int lengthofCollatzSequence(int i){
  int count = 0;
    while(i != 1) {
      if(i % 2 == 0){
        i = i / 2;
        count++;
      }
      else{
        i = ((3*i) + 1);
        count++;
      }
    }
  return count;
}


int main(int argc, char *argv[]){
  int a, b, count, maxi;
  int maxCount = 0;
  scanf("%d %d", &a, &b);
  for(int i = a; i <= b; i++){
    count = lengthofCollatzSequence(i);
    if(maxCount < count){
      maxCount = count;
      maxi = i;
    }
  }
  printf("%d\n", maxi);
return 0;
}
