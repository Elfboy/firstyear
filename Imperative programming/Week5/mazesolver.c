#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

char **twoArray(int m, int n) {
    int i;
    char **array = malloc( m * sizeof(char *));
    assert( array != NULL );
    for ( i = 0; i < m; i++ ) {
        array[i] = calloc(n, sizeof(char));
        assert(array[i] != NULL );
    }
    return array;
}

void freeArray(int m, char **array) {
    int i;
    for (i = 0; i < m; i++) {
        free(array[i]);
    }
    free(array);
}


char pathfinder(char **maze, int currentx, int currenty){
	
	if(maze[currentx][currenty] == 'E'){
		return **maze;
	}
	//rechts
	if(maze[currentx][currenty+1] == '.' || maze[currentx][currenty+1] == 'E'){
		currenty = currenty+1;
		maze[currentx][currenty] = '#';
		printf("RIGHT:%d %d\n", currentx, currenty);
		pathfinder(maze, currentx, currenty);
	}
	//omlaag
	else if(maze[currentx+1][currenty] == '.' || maze[currentx+1][currenty] == 'E'){
		currentx = currentx+1;
		maze[currentx][currenty] = '#';
		printf("DOWN:%d %d\n", currentx, currenty);
		pathfinder(maze, currentx, currenty);
	}
	//links
	else if(maze[currentx][currenty-1] == '.' || maze[currentx][currenty-1] == 'E'){
		currenty = currenty-1;
		maze[currentx][currenty] = '#';
		printf("LEFT%d %d\n", currentx, currenty);
		pathfinder(maze, currentx, currenty);
	}
	//boven
	else if(maze[currentx-1][currenty] == '.' || maze[currentx-1][currenty] == 'E'){
		currentx = currentx-1;
		maze[currentx][currenty] = '#';
		printf("UP:%d %d\n", currentx, currenty);
		pathfinder(maze, currentx, currenty);
	}

}


int main(int argc, char *argv[]){
	int n,m,currentx, currenty;
	scanf("%d %d\n", &n, &m);
	char **maze = twoArray(n,m);

	for(int i = 0; i < n; i++){
		for(int j = 0; j < m ; j++){
			scanf("%c", &maze[i][j]);
			if(maze[i][j] == 'S'){
				currentx = i;
				currenty = j;
			}
		}
		scanf("\n");
	}
	pathfinder(maze, currentx, currenty);

		for(int i = 0; i < n; i++){
		for(int j = 0; j < m ; j++){
			printf("%c", maze[i][j]);
		}
		printf("\n");
	}
	
	return 0;
}
