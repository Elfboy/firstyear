#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>
// #include <time.h>

#define DEFAULT_MAXNUM   100000000

static uint16_t *result;

int collatz(long n)
{
    long originalNumber = n;
    long count          = 1;

    while (1) {
        if (n & 1) {
            n = n*3+1;
        } else {
            n >>= 1;
            if (n < originalNumber)
                return count + result[n];
        }
        count++;
    }
}

int main(int argc, char *argv[])
{
    int n = 0, a, b, len=-1;
    long maxNum = DEFAULT_MAXNUM;
    scanf("%d %d", &a, &b);
    
    if(a < 0 || b < 0){
      printf("%d\n", n);
      return 0;
    }
    if(b > 100000000){
      printf("%d\n", n);
      return 0;
    }

    if(a == b){
      printf("%d\n", a);
      return 0;
    }

    result = malloc(maxNum * sizeof(result[0]));

    // clock_t begin = clock();

    result[1] = 1;

    for(int i=a+1; i<=b; i++) {
        int l = result[i] = collatz(i);
        if (l > len) {
            len = l;
            n  = i;
        }
    }

// clock_t end = clock();
// double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

     printf("%d\n", n);

    return 0;
}
