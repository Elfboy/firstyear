#include <stdio.h>
// #include <time.h>
#define MAX64 9223372036854775807L /* 2Ë†63 -1 */
int array[100000000] = {0};

int lengthCollatz(long n) {

  if(n == 1){
    return 0;
  }
  if(n < 100000000){
    if(array[n] == 0){
      if(n % 2 == 0){
        array[n] = 1 + lengthCollatz(n/2);
        return array[n];
      }
      else{
        array[n] = 1 + lengthCollatz((n*3) + 1);
        return array[n];
      }
    }
    else{
      return array[n];
    }
  }
  else{
    if(n%2 == 0){
      return 1 + lengthCollatz(n/2);
    }
    else{
      return 1 + lengthCollatz((n*3) + 1);
    }
  }
}

int main(int argc, char *argv[]) {
  int n, a, b, len=-1;
  scanf ("%d %d", &a, &b);
  
  // clock_t begin = clock();

  while (a <= b) {
    // printf("A:%d\n", a);
    int l = lengthCollatz(a);
    // printf("LENGTH: %d\n", l);
    if (l > len) {
      n = a;
      len = l;
  }
    a++;
  }

// clock_t end = clock();
// double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

  printf("%d\n", n);
  return 0;
}
