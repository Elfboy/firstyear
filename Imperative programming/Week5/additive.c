#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int func(int bound, int cur, int sum){
	while(cur <= bound){
		return (func(bound, cur+1, sum+cur) + func(bound, cur+1, sum-cur));
	}
	 if(sum == bound){
		return 1;
	}
	
	return 0;
}


int main(int argc, char *argv[]){

	int n, answer;
	scanf("%d", &n);
	answer = func(n, 2, 1);
	printf("%d\n", answer);

	return 0;
}
