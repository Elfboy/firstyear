/* file:	hello.c						*/
/* author:	elias_1233@hotmail.com		*/
/* date:	06-09-2016					*/
/* version:	1.0							*/
/* Description:	Calculate best time */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

int main(int argc, char *argv[]) {

	int a,b,c;
	double min, minMin, max, maxMax, middle, Sum = 0;

	scanf("%d %d %d", &a, &b, &c);
	
		min = MIN(a,b);
		minMin = MIN(min, c);
		max = MAX(a,b);
		maxMax = MAX(max,c);
		middle = (a + b + c) - (minMin + maxMax);

		Sum = minMin;
		Sum += minMin + middle;
		Sum += minMin + middle + maxMax;

		Sum = Sum / 3;
	printf("%.1f\n", Sum);

	return 0;
}
