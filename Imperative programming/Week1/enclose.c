/* file:	hello.c						*/
/* author:	elias_1233@hotmail.com		*/
/* date:	06-09-2016					*/
/* version:	1.0							*/
/* Description:	This program give the enclosed number of an input */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) {
	int n, m = 0, lastDigit = 0;

	scanf("%d", &n);

	lastDigit = n % 10;
	m = n;

	while(m >= 10) {
		m = m / 10;
	}

	printf("%d%d encloses %d.\n", m, lastDigit, n);
	
	return 0;

}
