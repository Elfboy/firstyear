/* file:	hello.c						*/
/* author:	elias_1233@hotmail.com		*/
/* date:	06-09-2016					*/
/* version:	1.0							*/
/* Description:	This program computes how many packs of paper are needed */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) {
	int pack = 0;
	int n;

	scanf("%d", &n);
	
	while(n > 0) {
		n = n - 500;
		pack++;
	}

	printf("%d\n", pack);
	
	return 0;
}
