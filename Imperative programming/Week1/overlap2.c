/* file:	hello.c						*/
/* author:	elias_1233@hotmail.com		*/
/* date:	06-09-2016					*/
/* version:	1.0							*/
/* Description:	Find the overlapping rectangles */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


	int main(int argc, char *argv[]) {
	
	int x1, y1, x2, y2, X1, Y1, X2, Y2;
    scanf("%d%d%d%d",&x1, &y1, &x2, &y2);
    scanf("%d%d%d%d", &X1, &Y1, &X2, &Y2);

    if(x1 > x2) {
    	int a;
    	a = x1;
    	x1 = x2;
    	x2 = a;
    }
    if(X1 > X2) {
    	int a;
    	a = X1;
    	X1 = X2;
    	X2 = a;
    }
    if(y1 > y2) {
    	int a;
    	a = y1;
    	y1 = y2;
    	y2 = a;
    }
    if(Y1 > Y2) {
    	int a;
    	a = Y1;
    	Y1 = Y2;
    	Y2 = a;
    }

     if((x1 <= X1) && (x2 >= X2) && (y1 <= Y1) && (y2 >= Y2)) {
    	printf("overlap\n");
    	return 0;
    }

    if((X1 <= x1) && (X2 >= x2) && (Y1 <= y1) && (Y2 >= y2)) {
    	printf("overlap\n");
    	return 0;
    }

    if((x2 < X1) || (X2 < x1)) {
        printf("no overlap\n");
    }
   else if ((y1 > Y2) || (Y1 > y2)){
    printf("no overlap\n");
   }

    if((!(x2 < X1)) && (!(X2 < x1))) {
    	printf("overlap\n");
    }

    return 0;
}
