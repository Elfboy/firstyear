/* file:	hello.c						*/
/* author:	elias_1233@hotmail.com		*/
/* date:	06-09-2016					*/
/* version:	1.0							*/
/* Description:	This program prints 'Hello world' 	*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) {
	printf("Hello world!\n");
	return 0;
}
