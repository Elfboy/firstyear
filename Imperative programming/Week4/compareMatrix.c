/* file:  compareMatrix.c         */
/* author:  elias_1233@hotmail.com    */
/* date:  11-10-2016          */
/* version: 1.0             */
/* Description: This program compares if two matrices are equal or not*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int **twoArray(int m, int n) {
    int i;
    int **array = malloc( m * sizeof(int *));
    assert( array != NULL );
    for ( i = 0; i < m; i++ ) {
        array[i] = calloc(n, sizeof(int));
        assert( array[i] != NULL );
    }
    return array;
}

void freeArray(int m, int **array) {
    int i;
    for (i = 0; i < m; i++) {
        free(array[i]);
    }
    free(array);
}

int getNumber(int *ch) {
    // printf("/%d\n", *ch);
    *ch = getchar();
    int num = *ch - 48;
    *ch = getchar();
    while (*ch != ' ' && *ch != '\n') {
        num = num * 10 + *ch - 48;
        *ch = getchar();
    }
    return num;
}



int main() {

    int size, ch, zeroFlag = 0, arrayFlag;
    size = getNumber(&size);
    int **array = twoArray(size, size);
    int **array2 = twoArray(size, size);

    for (arrayFlag = 0; arrayFlag < 2; arrayFlag++) {
        for (int i = 0; i < size; i++ ) {
            int j = 0;
            do {
                int input = getNumber(&ch);
                if (input == 0) {
                    input = getNumber(&ch);
                    for (int k = 0; k < input; k++ ) {
                        if (arrayFlag == 0)
                            array[i][j] = 1;
                        else
                            array2[j][i] = 1;
                        j++;
                    }
                } else {
                    if (zeroFlag == 0) {
                        for (int k = 0; k < input; k++ ) {
                            if (arrayFlag == 0)
                                array[i][j] = 0;
                            else
                                array2[j][i] = 0;
                            j++;
                        }
                        zeroFlag = 1;
                    } else {
                        for (int k = 0; k < input; k++ ) {
                            if (arrayFlag == 0)
                                array[i][j] = 1;
                            else
                                array2[j][i] = 1;
                            j++;
                        }
                        zeroFlag = 0;
                    }
                }
            } while (ch != '\n');
            zeroFlag = 0;
        }
        getchar();
    }

    int returnflag = 0;

    for (int i = 0 ; i < size; i++ ) {
        for (int j = 0; j < size; j++ ) {
            if (array[i][j] != array2[i][j]) {
                returnflag++;
                break;
            }
        }
    }

    (returnflag == 0 ? printf("EQUAL\n") : printf("DIFFERENT\n"));

    freeArray(size, array2);
    freeArray(size, array);

    return 0;
}