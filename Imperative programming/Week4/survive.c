/* file:  powerful.c           */
/* author:  elias_1233@hotmail.com    */
/* date:  19-09-2016          */
/* version: 1.0             */
/* Description: This program give the number that is equal to the sum of it own numbers to the power of ammount of digits*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

void *safeMalloc(int n) { void *p = malloc(n); if (p == NULL) {
printf("Error: malloc(%d) failed. Out of memory?\n", n); exit(EXIT_FAILURE);
}
return p; }


int main(int argc, char *argv[]) { 
    int *arr; 
    int m = 0, i, n = 2500;
    arr = safeMalloc(5000 * sizeof(int));

    for(i = 0; i < n; i++) {
        arr[i] = 2*i+1;
    }
    
    	m = arr[1];
		
	for(int j = m; j <= n; j+=m){
		arr[j - 1] = 0;
	}
	int count = 0;
	for(int i=0; i <=5000; i++){
		if (arr[i]==0)
			count++;
		arr[i]=arr[i+count];
	}
	
	 for(i = 0; i < n; i++) {
        printf("%d\n", arr[i]);
    }

    return 0; 
    }
