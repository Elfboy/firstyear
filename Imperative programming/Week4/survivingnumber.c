/* file:  survinvingnumber.c           */
/* author:  elias_1233@hotmail.com    */
/* date:  11-10-2016          */
/* version: 1.0             */
/* Description: This program gives the surviving numbers from a sieving process*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    int size = 5000;
    int arr[size];
    for ( int i = 0; i < 5000; i++ ) {
        arr[i] = 2 * i + 1;
    }

    int n = 1;
    int m, idx;

    while (n < size) {
        m = arr[n];
        int j = 0;
        for ( idx = 0; j < size; idx++ ) {
            if ((j + 1) % m == 0)
                j++;
            arr[idx] = arr[j];
            j++;
        }
        size = idx;
        n++;
    }

    int input;
    scanf("%d", &input);
    printf("%d\n", arr[input - 1]);

    return 0;
}