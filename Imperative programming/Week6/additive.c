#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int func(int bound, int cur, int sum, int counter){

	if(cur <= bound && counter == 5){
			counter = 0;
			return (func(bound, cur+1, sum+cur, 0) + func(bound, cur+1, sum-cur, 0)  + func(bound, cur+2, (sum - (cur * (cur+1))), counter+1) + func(bound, cur+2, (sum + (cur * (cur+1))), counter+1));
		}
	if(cur <= bound){
		return (func(bound, cur+1, sum+cur, 0) + func(bound, cur+1, sum-cur, 0)  + func(bound, cur+2, (sum + (cur * (cur+1))), 0) + 
			func(bound, cur+2, (sum - (cur * (cur+1))), 0))+ func(bound, cur+2, (sum * (cur * (cur+1))), counter+1);
	}
	if(sum == bound){
		return 1;
	}
	

	
	return 0;
}


int main(int argc, char *argv[]){

	int n, answer;
	scanf("%d", &n);
	answer = func(n, 2, 1, 0);
	printf("%d\n", answer);

	return 0;
}


