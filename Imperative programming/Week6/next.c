#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
 
char **twoArray(int m, int n) {
    int i;
    char **array = malloc( m * sizeof(char *));
    assert( array != NULL );
    for ( i = 0; i < m; i++ ) {
        array[i] = calloc(n, sizeof(char));
        assert(array[i] != NULL );
    }
    return array;
}
 
void freeArray(int m, char **array) {
    int i;
    for (i = 0; i < m; i++) {
        free(array[i]);
    }
    free(array);
}
 
// void function that prints the array
void printArray (char **maze, int heigth, int width){
    for(int i = 0; i < heigth; i++){
        for(int j = 0; j < width; j++){
            printf("%c", maze[i][j]);
        }
        printf("\n");
    }
 
}
 
void pathfinder(char **maze, int currentx, int currenty, int heigth, int width){
 
    char copyChar = maze[currentx][currenty];
   
    maze[currentx][currenty] = '#';
       
    // check for E right
    if( currentx+1 <= heigth && maze[currentx+1][currenty] == 'E' ){
        maze[currentx+1][currenty] = '#';
        printArray(maze,heigth,width);
    }
   
    // check for E down
    else if(currenty+1 <= width && maze[currentx][currenty+1] == 'E'){
        maze[currentx][currenty+1] = '#';
        printArray(maze,heigth,width);
    }
   
    // check for E left
    else if( currentx-1 > 0 && maze[currentx-1][currenty] == 'E'){
        maze[currentx-1][currenty] = '#';
        printArray(maze,heigth,width);
    }
   
    // check for E up
    else if(currenty-1 > 0 && maze[currentx][currenty-1] == 'E'){
        maze[currentx][currenty-1] = '#';
        printArray(maze,heigth,width);
    }
    else{
        if( currentx+1 <= heigth && maze[currentx+1][currenty] == '.'){
            // go into recursion right
            pathfinder(maze,currentx+1, currenty, heigth, width);
        }
        if(currenty+1 <= width && maze[currentx][currenty+1] == '.'){
            // go into recursion down
            pathfinder(maze,currentx, currenty+1, heigth, width);
        }
        if(currentx-1 > 0 && maze[currentx-1][currenty] == '.'){
            // go into recursion left
            pathfinder(maze,currentx-1, currenty, heigth, width);
        }
        if(currenty-1 > 0 && maze[currentx][currenty-1] == '.'){
            // go into recursion up
            pathfinder(maze,currentx, currenty-1, heigth, width);
        }      
       
    }
   
    maze[currentx][currenty] = copyChar;
   
}  
 
int main(int argc, char *argv[]){
    int n,m,currentx, currenty;
    scanf("%d %d\n", &n, &m);
    char **maze = twoArray(n,m);
 
    for(int i = 0; i < n; i++){
        for(int j = 0; j < m ; j++){
            scanf("%c", &maze[i][j]);
            if(maze[i][j] == 'S'){
                currentx = i;
                currenty = j;
            }
        }
        scanf("\n");
    }
   
    pathfinder(maze, currentx, currenty, n, m);
    

    return 0;
}
