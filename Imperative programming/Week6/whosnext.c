 #include <stdio.h>
 #include <stdlib.h>

char nextPermutation(char *string, int length) {
	int i, j, temp;
	if (length == 0){
		return 0;
	}
	i = length - 1;
	while ((i > 0) && (string[i-1] >= string[i])){
		i--;
		if (i == 0){
			return 0;
		}
	}

	j = length - 1;
	while (string[j] <= string[i-1])
		j--;
		temp = string[i - 1];
		string[i-1] = string[j];
		string[j] = temp;
	
	j = length - 1;
	while (i < j) {
		temp = string[i];
		string[i] = string[j];
		string[j] = temp;
		i++;
		j--;
	}
	return *string;
}

int main(int argc, char *argv[]){
	int size = 128, currentSize = 0;
    char *string = malloc(size);
    currentSize = size;

    if(string != NULL){
    int c, i = 0;

	while (( c = getchar() ) != '.'){
		string[i]= c;
		i++;
		
		if(i == currentSize){
            currentSize = i+size;
			string = realloc(string, currentSize);
		}
	}
	nextPermutation(string, i);

	for(int j = 0; j < i; j++){
		printf("%c",string[j]);
	}
	printf("\n");
    }
	return 0;
}