#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

int **twoArray(int m, int n) {
    int i;
    int **array = malloc( m * sizeof(int *));
    assert( array != NULL );
    for ( i = 0; i < m; i++ ) {
        array[i] = calloc(n, sizeof(int));
        assert( array[i] != NULL );
    }
    return array;
}

void freeArray(int m, int **array) {
    int i;
    for (i = 0; i < m; i++) {
        free(array[i]);
    }
    free(array);
}

void genBitComb(int **array, int size, int width){
	int i, j, n, counter, bit;
	n = size/2;
	for(i=0; i < width; i++){
		bit=0;
		counter = 0;
		for(j=0; j < size; j++){
			array[i][j] = bit;
			
			if((j+1) % n == 0){
				counter++;
			}
			
			if(counter % 2 == 0 ){
				bit = 0;
			}
			else{
				bit = 1;
			}
			
		}
		n /= 2;
	}
}

void switchBits(int **bitComb, int finalSize, int row){
	int i;
	for(i = 0; i < finalSize; i++){
		if(bitComb[row][i]){
			bitComb[row][i] = 0;
		}
		else{
			bitComb[row][i]=1;
		}
	}
}

void printBitComb(int **bitComb, int m, int finalSize){
	int i , j, flag=0;
	for(i = 0; i < finalSize; i++){
		if(bitComb[m][i]){
			for(j = 0; j < m; j++){
				printf("%d", bitComb[j][i]);
			}
			printf("\n");
			flag=1;
		}
	}
	if(flag == 0){
		printf("UNSATISFIABLE\n");
	}
}
int main(int argc, char* argv[]){
	int m, n, i,j,c,finalSize, flag, value, conjunct;
	int **bitComb, *numbers, *negs;
	scanf("%d %d\n", &m, &n);
	finalSize=pow(2,m);
	flag=n;
	
	bitComb=twoArray(m+1, finalSize);
	genBitComb(bitComb, finalSize, m);
	numbers= malloc(m*sizeof(int));
	negs= malloc(m*sizeof(int));
	
	for(i = 0; i < finalSize; i++){
		bitComb[m][i] = 1;
	}
	
	while(flag>0){
		c=getchar();
		while(1){
			c=getchar();
	
			if(c == '~'){
				c = getchar();
				scanf("%d", &value); 
				negs[value] = 1; 
				numbers[value] = 1;
			}
			else{
				scanf("%d", &value); 
				numbers[value]=1; 
			}
			c=getchar();
			if(c==']'){
				break;
			}
		}

		for(i = 0; i < m; i++){
			if(negs[i] == 1){
				switchBits(bitComb, finalSize, i);
			}
		}

		for(j = 0; j < finalSize; j++){
			conjunct = 0;
			for(i=0; i<m; i++){
				if(numbers[i]){
					conjunct += bitComb[i][j];
				}
			}
			
			if(conjunct == 0){
				bitComb[m][j]=0;
			}
		}
		
		for(i = 0; i < m; i++){
			if(negs[i]){
				switchBits(bitComb, finalSize, i);
			}
		}		
		
		for(i = 0; i < m; i++){
			numbers[i] = 0;
			negs[i] = 0;
		}
		c=getchar();
		flag--;
	}
	
	printBitComb(bitComb, m, finalSize);
	freeArray(m+1, bitComb);
	return 0;
}
