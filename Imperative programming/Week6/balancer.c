#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[])
{
  char s[1000000], *p, *q;
  int i, n, *v, lowercounter = 0, highercounter = 0, middlecounter = 0, mincounter = 1000;
  fgets(s, sizeof s, stdin);
  for (p = s, n = 0; ; p = q, n++) {
    strtol(p, &q, 10);
    if (p == q) 
      break;
    }

  v = malloc(sizeof(int) * n);

  for (p = s, i = 0; i < n; i++) {
    v[i] = strtol(p, &p, 10);
  }

  for(int j = 0; j < n - 1; j++){
    for(int i = 0; i < n-1; i++){
    	if(v[j] < v[i]){
    		lowercounter++;
    	}
    	else if(v[j] > v[i]){
    		highercounter++;
    	}
    	else if(v[j] == v[i]){
    		middlecounter++;
    	}
   	}
   	if((lowercounter == highercounter) && (v[j] < mincounter)){
   		mincounter = v[j];
   	}
   	highercounter = middlecounter = lowercounter = 0;
	}

	if(mincounter == 1000){
		printf("UNBALANCED\n");
	}
  else{
	printf("%d\n", mincounter);
  }
  free(v);
  return 0;
}
