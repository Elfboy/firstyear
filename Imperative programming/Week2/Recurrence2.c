/* file:  hello.c           */
/* author:  elias_1233@hotmail.com    */
/* date:  06-09-2016          */
/* version: 1.0             */
/* Description: This program give the enclosed number of an input */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


void recursion(int n, int ans, int count) {
  for(int i = 0; i < count+1; i++){
      printf("f(");
  }
  printf("%d", n);
  for(int i = 0; i < count+1; i++){
      printf(")");
  }
  printf("=");

    if(n > 100) { 
        if(count == 0){
          ans = n -10;
          printf("%d\n", ans);
        }
        else {
          recursion(n - 10, ans, count - 1);
        }
        
      }
       else{
          recursion(n + 11, ans, count + 1);
        }  
    }


int main(int argc, char *argv[]) {

      int n, ans = 0, count = 0;
      scanf("%d", &n);
      recursion(n, ans, count);
      return 0;
  }
