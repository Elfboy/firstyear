/* file:  Recurrence.c           */
/* author:  elias_1233@hotmail.com    */
/* date:  19-09-2016          */
/* version: 1.0             */
/* Description: computes a recursive function of integers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) {

  int n, count = 1;
  scanf("%d", &n);
  /*Printing the functions*/
  while(count > 0) {
    for(int i = 0; i < count; i++){
      printf("f(");
    }

    printf("%d", n);

    for(int i = 0; i < count; i++){
      printf(")");
    }
  
    printf("=");
    /*Recursive functions*/
    if(n > 100) {
      n = n - 10;
      count--;
    }

    else{
      n = n + 11;
      count++;
      }
  }
  printf("%d\n", n);
  return 0;
}
