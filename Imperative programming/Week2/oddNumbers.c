/* file:  oddNumbers.c           */
/* author:  elias_1233@hotmail.com    */
/* date:  19-09-2016          */
/* version: 1.0             */
/* Description: This program selects the odd numbers of an input */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) {

  int n, odd=0, count=0;
  int reversedNumber = 0, remainder;
  scanf("%d", &n);

  /*Reverse the inputed number*/
  while(n != 0)  {
    remainder = n%10;
    reversedNumber = reversedNumber*10 + remainder;
    n /= 10;
  }

  /*finds all the odd numbers*/
  while(reversedNumber > 0){
    odd = reversedNumber % 10;
    if(odd % 2 == 1) {
      printf("%d",odd);
      count++;
    }
    reversedNumber =reversedNumber/10;
  }
  if(count == 0){
    printf("0");
  }
  printf("\n");
  return 0;
  }
