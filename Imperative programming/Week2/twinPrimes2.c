/* file:  twinPrimes2.c          */
/* author:  elias_1233@hotmail.com    */
/* date:  19-09-2016          */
/* version: 1.0             */
/* Description: This program gives the pairs of primes that are twins */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*Fuction to check if a number is a prime*/
int isPrime(int n){
  int i, twinPrime = n+2;

  for(i = 3; i*i <= twinPrime; i+=2){
    if(((n%i) == 0) || (twinPrime%i) == 0) {
      return 0;
    }
  }
  return 1;
}

int main(int argc, char *argv[]) {

  int n, counter = 0, number = 3;
  scanf("%d", &n);

  while(counter < n) {
    if(isPrime(number)) {
      counter++;
    }
    if(counter == n) {
      printf("%d %d\n", number, number+2);
    }
    number += 2;
  }
  return 0;
}  
