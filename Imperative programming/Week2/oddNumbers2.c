/* file:  hello.c           */
/* author:  elias_1233@hotmail.com    */
/* date:  06-09-2016          */
/* version: 1.0             */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


void recursion(int number) {
    if(number / 10 == 0){
      if(number % 2 == 1)
        printf("%d",number);
    }
    else
    {
      recursion(number/10);
      if ((number % 10)%2 == 1)
        printf("%d",number %10 );    
    }
       }


int main(int argc, char *argv[]) {

      int number=0;
      scanf("%d", &number);
      recursion(number);
       printf("\n");

      return 0;
  }
