/* file:  powerful.c           */
/* author:  elias_1233@hotmail.com    */
/* date:  19-09-2016          */
/* version: 1.0             */
/* Description: This program give the number that is equal to the sum of it own numbers to the power of ammount of digits*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*Function for computing the power of*/
int power(int a, int b) {
  
  int result = 1;
  int power = b;
  int base = a;
  while(power > 0) {
    if(power % 2 == 1){
      result = result * base;
    }
    base = base * base;
    power = power/2;
  }
  return result;
}

int ispowerFul(int num) {

  int lastDigit, tempNum, tempNum1, digits = 0, sum = 0;
  tempNum1 = num;
  tempNum = num;
  /*Calculates the ammount of digits*/
  while(tempNum1 > 0){
    tempNum1 = tempNum1/10;
    digits++;
  }
  /*Checks if it has a powerfull property*/
  while(tempNum > 0){
    lastDigit = tempNum%10;
    sum = sum + power(lastDigit,digits);
    tempNum = tempNum/10;
}

 if(sum == num){
    return 1;
  }
  else{
    return 0;
  }
}


int main(int argc, char *argv[]) {  

  int n, counter = 0, number = 1;
  scanf("%d", &n);

  while(counter != n){
    if(ispowerFul(number)){
        counter++;
    }
    if(counter == n){
        printf("%d\n", number);
    }
    number++;
  }
  return 0;
}
