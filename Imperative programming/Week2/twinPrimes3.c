/* file:  hello.c           */
/* author:  elias_1233@hotmail.com    */
/* date:  06-09-2016          */
/* version: 1.0             */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int isPrime(int n){
  int i;
  if((n%2) == 0){
    return 0;
  }
  for(i = 3; i*i <= n/2; i +=2) {
    if((n % i) == 0){

      return 0;
    }
  }
  return 1;
}

int main(int argc, char *argv[]) {

    int n, counter = 0, number = 3;
    scanf("%d", &n);

    while(counter < n) {
      if(isPrime(number) && isPrime(number+2)) {
        counter++;
        }
        if(counter == n) {
          printf("%d %d\n", number, number+2);
          }
        number += 2;
    }
     return 0;
}  
