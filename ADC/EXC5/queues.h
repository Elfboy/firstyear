#ifndef QUEUES_H

#define QUEUES_H

typedef struct ListNode *List;  /* List is the type of lists of states */

typedef struct Location {
  int x;
  int y;
  int dist;
} Location;

struct ListNode {
  Location item;
  List next;
};

typedef struct Queue { /* a queue is a list and a pointer to the last node */
  List list;
  List lastNode;
} Queue;

List newEmptyList();
int isEmptyList (List li);
void listEmptyError();
List addItem(Location loc, List li);
Location firstItem(List li);
List removeFirstNode(List li);
void freeList(List li);

Queue newEmptyQueue ();
int isEmptyQueue (Queue q);
void emptyQueueError ();
void enqueue (Location loc, Queue *qp);
Location dequeue(Queue *qp);
void freeQueue (Queue q);

#endif
