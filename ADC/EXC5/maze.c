#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "queues.h"

typedef enum Option {
  Gate,
  Flowerbed,
  None
} Option;

int min(int a, int b) {
  return (a <= b ? a : b);
}

int isMazeChar (char c) {
  if (!(c == '#' || c == '.' || c == '%' || c == '^')) {
    return 0;
  }
  return 1;
}

int** int2dArr (int r, int c) {
  int i;
  int** arr = malloc(r * sizeof(int*));
  assert(arr != NULL);
  
  int* rows = calloc(r * c, sizeof(int));
  assert(rows != NULL);
  
  for (i = 0; i < r; i++) {
    arr[i] = rows + i * c;
  }
  return arr;
}

char** readMaze(int r, int c){
  int i, j;
  char square;
  
  char** maze = malloc(r * sizeof(char*));
  assert(maze != NULL);
  char* rows = malloc(r * c * sizeof(char));
  assert(rows != NULL);
  
  for (i = 0; i < r; i++) {
    maze[i] = rows + i * c;
    j = 0;
    while (j < c) {
      scanf("%c", &square);
      if (isMazeChar(square)) {
        maze[i][j] = square;
        j++;
      }
    }
  }
  return maze;
}

int* inspectOptions(int k, int hgate, int hflower) {
  /* Gate = 0, Flowerbed = 1, None = 2 */
  int i, height;
  int* options = calloc(3, sizeof(int));  
  assert(options != NULL);
  for (i = 0; i < k; i++) {
    scanf("%d", &height);
    if (height <= hgate) {
      options[Gate] = 1;
    } else if (height >= hflower) {
      options[Flowerbed] = 1;
    } else {
      options[None] = 1;
    }
  }
  return options;
}

int canUseField (Option opt, char field) {
  switch (opt) {
    case None:
      if (field == '.') {
        return 1;
      }
      break;
    case Gate:
      if ((field == '.') || (field == '^')) {
        return 1;
      }
      break;
    case Flowerbed:
      if ((field == '.') || (field == '%')) {
        return 1;
      }
      break;
  }
  return 0;
}

int tryOption(Option option, char** maze, int r, int c) {
  int x, y, dist;
  int** visited = int2dArr(r, c);
  
  Location loc;
  loc.x = c/2;
  loc.y = r/2;
  loc.dist = 1;
  
  Queue queue = newEmptyQueue();
  enqueue(loc, &queue);
  visited[loc.y][loc.x] = 1;
  
  while (!isEmptyQueue(queue)) {
    loc = dequeue(&queue);
    
    x = loc.x;
    y = loc.y;
    dist = loc.dist;
    if (((x == 0) || (x == c - 1) || (y == 0) || (y == r - 1)) && (canUseField(option, maze[y][x]))) {
      freeQueue(queue);
      free(visited[0]);
      free(visited);
      return dist;
    }
    
    if ((canUseField(option, maze[y + 1][x])) && (!visited[y + 1][x])) {
      loc.y = y + 1;
      loc.x = x;
      loc.dist = dist + 1;
      enqueue(loc, &queue);
      visited[loc.y][loc.x] = 1;
    }
    
    if ((canUseField(option, maze[y - 1][x])) && (!visited[y - 1][x])) {
      loc.y = y - 1;
      loc.x = x;
      loc.dist = dist + 1;
      enqueue(loc, &queue);
      visited[loc.y][loc.x] = 1;
    }
    
    if ((canUseField(option, maze[y][x + 1])) && (!visited[y][x + 1])) {
      loc.y = y;
      loc.x = x + 1;
      loc.dist = dist + 1;
      enqueue(loc, &queue);
      visited[loc.y][loc.x] = 1;
    }
    
    if ((canUseField(option, maze[y][x - 1])) && (!visited[y][x - 1])) {
      loc.y = y;
      loc.x = x - 1;
      loc.dist = dist + 1;
      enqueue(loc, &queue);
      visited[loc.y][loc.x] = 1;
    }
  }
  return -1;
}

int main (int argc, char *argv[]) {
  int n, hgate, hflower, k, r, c, i;
  char** maze;
  
  scanf("%d", &n);
  for (i = 0; i < n; i++) {
    scanf("%d %d", &hgate, &hflower);
    scanf("%d", &k);
    int* options = inspectOptions(k, hgate, hflower);
    scanf("%d %d", &r, &c);
    maze = readMaze(r, c);
    
    int shortestPath = 1000 * 1000;
    
    if (options[Gate]) {
      shortestPath = min(shortestPath, tryOption(Gate, maze, r, c));
    }
    
    if (options[Flowerbed]) {
      shortestPath = min(shortestPath, tryOption(Flowerbed, maze, r, c));
    }
    
    if (options[None]) {
      shortestPath = min(shortestPath, tryOption(None, maze, r, c));
    }
    
    printf("%d\n", shortestPath);
    
    free(options);
    free(maze[0]);
    free(maze);
  }
  return 0;
}
