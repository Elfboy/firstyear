#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "queues.h"

List newEmptyList() {
  return NULL;
}

int isEmptyList (List li) {
  return ( li==NULL );
}

void listEmptyError() {    
  printf("list empty\n");
  abort();
}

List addItem(Location loc, List li) {
  List newList = malloc(sizeof(struct ListNode));
  assert(newList!=NULL);
  newList->item = loc;
  newList->next = li;
  return newList;
}

Location firstItem(List li) {
  if ( li == NULL ) {
    listEmptyError();
  }
  return li->item;
}

List removeFirstNode(List li) {
  List returnList;
  if ( li == NULL ) {
    listEmptyError();
  }
  returnList = li->next;
  free(li);
  return returnList;
}

void freeList(List li) {
  List li1;
  while ( li != NULL ) {
    li1 = li->next;
    free(li);
    li = li1;
  }
  return;
}


Queue newEmptyQueue () {
  Queue q;
  q.list = newEmptyList();
  q.lastNode = NULL;
  return q;
}

int isEmptyQueue (Queue q) {
  return isEmptyList(q.list);
}

void emptyQueueError () {
  printf("queue empty\n");
  exit(0);
}
   
void enqueue (Location loc, Queue *qp) {
  if ( isEmptyList(qp->list) ) {
    qp->list = addItem(loc,NULL);
    qp->lastNode = qp->list;
  } else {
    (qp->lastNode)->next = addItem(loc,NULL);
    qp->lastNode = (qp->lastNode->next);
  }
}

Location dequeue(Queue *qp) {
  Location loc = firstItem(qp->list);
  qp->list = removeFirstNode(qp->list);
  if ( isEmptyList(qp->list) )  {
    qp->lastNode = NULL;
  }
  return loc;
}

void freeQueue (Queue q) {
	freeList(q.list);
}