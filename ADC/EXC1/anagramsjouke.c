#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

// new data type 'sentence' to store two elements.
// 'count' is to count the number of characters in a sentence.
// array 'alphabet[26]' is to count how many times which a letter is typed.
// 'alphabet[0]' is the number of 'a' in a typed sentence.
// 'alphabet[1]' is the number of 'b' in a typed sentence etc.
typedef struct {
  int alphabet[26];
  int count;
  int top;
  int counter;
  char* string;
} sentence;

void push (sentence n[], int m, char x){
  if (n[m].top != n[m].counter){
    n[m].string[n[m].counter] = x ;
    n[m].counter++;
  }
  else {
    doubleTheSize (n[m].string, n[m].top);
    n[m].string[n[m].counter]= x;
    n[m].top*=2;
    n[m].counter++;
  }
}

void doubleTheSize (chara— k,int topOf) {
int newSize = 2 a— topOf;
k = realloc(k, newSize a — sizeof(char));
assert(k != NULL);
}

int main(int argc, char** argv){
  // variable 'temp' is to store temporary input from the function 'getchar()'
  int n, temp;

  // 'cmp' is to allocate 'n' blocks of memory of the sentence type
  sentence* cmp;
  scanf("%d", &n);
  cmp = calloc(n,sizeof(sentence));
  assert(cmp!=NULL);
  for(int i = 0; i<n; i++){
    cmp[i].string = malloc(10*sizeof(char));
    assert(cmp[i].string!=NULL);
  }

  //'for' loop for storing the sentences.
  //each 'cmp[i].count' is initialized to 0.
  //'temp' become 0.
  //while loop that gets a charakter from the input and stops when a "." is reached
  //if the 'temp' is from 'a' up to 'z', the associated array 'cmp[i].alphabet[temp-97]is incremented '
  //the total number of characters is incremented by one in 'cmp[i].count'.
  for(int i=0; i<n; i++){
    cmp[i].count=0;
    temp = 0;
    while(temp!=46){
      temp=getchar();
      push (&cmp[i],temp);
      if(temp>=97&&temp<=122){
        cmp[i].alphabet[temp-97]++;
        cmp[i].count++;
      }
    }
  }
  //getting variable m (number of table sentences)
  //'sentence' type 'input' is to allocate 'm' blocks of memory whose size is the size of 'sentence'.
  int m;
  sentence* input;
  scanf("%d", &m);
  input = calloc(m,sizeof(sentence));
  assert(input!=NULL);
  // same tactic as storing the previous sentences
  for(int i=0; i<m; i++){
    input[i].count=0;
    temp = 0;
    while(temp!=46){
      temp=getchar();
      if(temp>=97&&temp<=122){
        input[i].alphabet[temp-97]++;
        input[i].count++;
      }
    }
  }

  //comparison of the sentences of table and input
  int check;
  for(int i=0; i<m; i++){
    for(int j=0; j<n ;j++){
      check=0;
      if(input[i].count==cmp[j].count){
        for(int k=0; k<26; k++){
          if(input[i].alphabet[k]!=cmp[j].alphabet[k]){
            check++;
            break;
          }
        }
        if(check==0){
          printf("%s \n is an anagram of \n %s",input[i].string,cmp[j].string);
          }
        }
      }
    }
    printf("\n");
  }

  //memory free
  free(cmp);
  free(input);

  return 0;
}
