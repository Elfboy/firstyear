// #include <stdio.h>
// #include <stdlib.h>
// #include <math.h>
// #include <assert.h>



// int main(int argc, char *argv[]){
// 	// int c, i = 0, position, n ,m;
// 	// char array[100];
// 	// int alpha[26] = {0};
// 	// scanf("%d", &n);
// 	// while((c = getchar()) != '.'){
// 	// 	if(c == ' '){
// 	// 		c = getchar();
// 	// 	}
// 	// 	array[i] = c;
// 	// 	i++;
// 	// }

// 	// for(int j = 0; j < i; j++){
// 	// 	position = array[j] - 65;
// 	// 	alpha[position]++;
// 	// }

// 	// for(int k = 0; k < 26; k++){
// 	// 	printf("%d ", alpha[k]);
// 	// }
// }
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct {
  int alphabet[26];
  int count;
} sentence;

int main(int argc, char** argv){
  int n, temp;
  sentence* cmp;
  scanf("%d", &n);
  cmp = calloc(n,sizeof(sentence));
  assert(cmp!=NULL);
  for(int i=0; i<n; i++){
    cmp[i].count=0;
    temp = 0;
    while(temp!=46){
      temp=getchar();
      if(temp>=97&&temp<=122){
        cmp[i].alphabet[temp-97]++;
        cmp[i].count++;
      }
    }

  }

  int m;
  sentence* input;
  scanf("%d", &m);
  input = calloc(m,sizeof(sentence));
  assert(input!=NULL);
  for(int i=0; i<m; i++){
    input[i].count=0;
    temp = 0;
    while(temp!=46){
      temp=getchar();
      if(temp>=97&&temp<=122){
        input[i].alphabet[temp-97]++;
        input[i].count++;
      }
    }
  }

  int check;
  for(int i=0; i<m; i++){
    for(int j=0; j<n ;j++){
      check=0;
      if(input[i].count==cmp[j].count){
        for(int k=0; k<26; k++){
          if(input[i].alphabet[k]!=cmp[j].alphabet[k]){
            check++;
            break;
          }
        }
        if(check==0){
          printf("%d ", j+1);
        }
      }
    }
    printf("\n");
  }

  free(cmp);
  free(input);

  return 0;
}