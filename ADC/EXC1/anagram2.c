#include <stdio.h>
#include <stdlib.h>


typedef struct{
	int letters[26];
	int count;
} Anagram;

int main(int argc, char *argv[]){

	int n, c, m;

	scanf("%d", &n);
	Anagram *array = malloc(n * sizeof(Anagram));

	for(int i = 0; i < n; i++){
		array[i].count = 0;
		while((c = getchar()) != '.'){
			if(c >= 97 && c <= 122){
				array[i].letters[c-97]++;
				array[i].count++;
			}
		}
	}
		scanf("%d", &m);
		Anagram *array2 = malloc(m * sizeof(Anagram));

		for(int i = 0; i < m; i++){
			array2[i].count = 0;
		while((c = getchar()) != '.'){

			if(c >= 97 && c <= 122){
				array2[i].letters[c-97]++;
				array2[i].count++;
			}
		}
	}

	int flag;
	for(int i = 0; i < m; i++){
		for(int j = 0; j < n; j++){
			flag = 0;
			if(array2[i].count == array[j].count){
				for(int k = 0; k < 26; k++){
					if(array2[i].letters[k] != array[j].letters[k]){
						flag++;
						break;
					}
				}
				if(flag==0){
					printf("%d ", j+1);
				}
			}
		}
		printf("\n");
	}

	free(array);
	free(array2);

}
