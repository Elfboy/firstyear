#include <stdio.h>  /* getchar, printf */
#include <stdlib.h> /* NULL */
#include <string.h> /* strcmp */
#include "scanner.h"
#include "recognizeExp.h"
#include <math.h>

/* The functions acceptNumber, acceptIdentifier and acceptCharacter have as 
 * (first) argument a pointer to an token list; moreover acceptCharacter has as
 * second argument a character. They check whether the first token
 * in the list is a number, an identifier or the given character, respectively.
 * When that is the case, they yield the value 1 and the pointer points to the rest of
 * the token list. Otherwise they yield 0 and the pointer remains unchanged.
 */

int acceptNumber(List *lp) {
  if (*lp != NULL && (*lp)->tt == Number) {
    *lp = (*lp)->next;
    return 1;
  }
  return 0;
}

int acceptIdentifier(List *lp) {
  if (*lp != NULL && (*lp)->tt == Identifier ) {
    *lp = (*lp)->next;
    return 1;
  }
  return 0;
}

int acceptCharacter(List *lp, char c) {
  if (*lp != NULL && (*lp)->tt == Symbol && ((*lp)->t).symbol == c ) {
    *lp = (*lp)->next;
    return 1;
  }
  return 0;
}

/* The functions acceptTerm, acceptExpression and acceptEquation have as 
 * argument a pointer to a token list. They check whether the token list 
 * has an initial segment that can be recognized as term, expression or equation, respectively.
 * When that is the case, they yield the value 1 and the pointer points to the rest of
 * the token list. Otherwise they yield 0 and the pointer remains unchanged.
 */

int acceptTerm(List *lp) { 
  if( acceptNumber(lp) ){
    if( acceptIdentifier(lp) ){
      if( acceptCharacter(lp, '^') ){
        if( !acceptNumber(lp) ){
          return 0;
        }
        return 1;
      }
      
    }
    return 1;   
  }
  return 0;
}

int acceptExpression(List *lp) {
  acceptCharacter(lp, '-');
  if ( !acceptTerm(lp) ) {
    return 0;
  }
  
  while ( acceptCharacter(lp,'+') || acceptCharacter(lp,'-') ) {
    if ( !acceptTerm(lp) ) {
      return 0;
    }
  }   /* no + or -, so we reached the end of the expression */
  return 1;
}

int acceptEquation(List *lp) {
  if( acceptExpression(lp) && acceptCharacter(lp,'=') && acceptExpression(lp) ) {
    return 1;
  }
  return 0;
}
/* converts small numbers */
float smallNumber(float x){
  if(x > -0.0005 && x < 0){
    x = x * -1;
     }
  return x;
}
/*return variable to other position*/
float rightPosition(float x){
   if(x < 0){
      x = x * -1;
      }
    else{
      x = x * -1;
    }
    return x;
}
/* Go through the tokenlist and find the highest degree */

int findHighestDegree (List li) {
  int degree=-1, powerOne=0;
  
  while(((li->next)->next) != NULL ){
    if((li->tt) == Identifier ){
      if( ((li->next)->tt) == Symbol &&
        ((li->next)->t).symbol == '^' &&
        (((li->next)->next)->tt) == Number ){ /* we found <identifier><symbol='^'><number>*/
          
        if( (((li->next)->next)->t).number > degree ){ /* update degree*/
          degree = (((li->next)->next)->t).number;
          }
        }
      else{
        powerOne=1;
      }
    }
    li = li->next;
  }
  if( degree <=0 && powerOne ){
    return 1;
  }
  return degree;
}

/* Go through the tokenlist and check if the equation is in 1 variable or not. 
 * returns 1 if it is in 1 variable, 0 otherwise */
int isOneVariable (List li) {
  char *variable;
  int foundIdentifier=0, first=1;
  
  while(li != NULL ){
    if((li->tt) == Identifier ){
      if( first ){
        first = 0;
        foundIdentifier = 1;
        variable = (li->t).identifier;
      }
      if(!strcmp( variable, (li->t).identifier ) ){ /* variables are the same*/
        variable = (li->t).identifier;
      }
      else{
        return 0;
      }
    }
    li = li->next;
  }
  if( !foundIdentifier ){
    return 0; /* false*/
  }
  return 1; /* true */
}

/* The next function can be used to demonstrate the recognizer.
 */
void findSolutionDegreeOne(List li){
  float a=0, b=0;
  int positive=1;

  while( (li->tt != Symbol) || (li->t).symbol != '='){
    if( (li->tt)==Number ){
      if( !positive ){
        (li->t).number = - (li->t).number;
        positive = 1;
      }

    if ( (li->next)->tt == Identifier ){
      if((((li->next)->next)) != NULL && ((li->next)->next)->tt == Symbol && (((li->next)->next)->t).symbol == '^' && (((li->next)->next)->next)->tt == Number ){
        if( ((((li->next)->next)->next)->t).number == 1){
            b += (li->t).number;
            li = (((li->next)->next)->next)->next;
        }
        else if(((((li->next)->next)->next)->t).number == 0){
          a -= (li->t).number;
          li = (((li->next)->next)->next)->next;
        }
      }
      else{
        b += (li->t).number;
        li = ((li->next)->next);
      }
    }
    else{
      a -= (li->t).number;
      li = (li->next);
    }
    }
    else{ 
      if( (li->t).symbol == '-' ){
        positive = 0;
      } 
      li = li->next;
    }
  }

  li = li->next;
  positive = 1;

  /* after '=' */
  while( li != NULL ){
    if((li->tt)==Number ){
      if( !positive ){
        (li->t).number = -(li->t).number;
        positive = 1;
      }
      if((li->next)!= NULL && ((li->next)->tt) == Identifier ){
        if( (((li->next)->next)) != NULL && ((li->next)->next)->tt == Symbol && (((li->next)->next)->t).symbol == '^' && (((li->next)->next)->next)->tt == Number ){
          if(((((li->next)->next)->next)->t).number == 1){
            b += (li->t).number;
            li = (((li->next)->next)->next)->next;
            }
          else if(((((li->next)->next)->next)->t).number == 0){
            a += (li->t).number;
            li = (((li->next)->next)->next)->next;
          }
        }
        else{
          b -= (li->t).number;
          li = (li->next)->next;
        }
      }
      else{
        a += (li->t).number;
        li = li->next;
      }
    }
    else{
      if( (li->t).symbol == '-' ){
        positive = 0;
      } 
      li = li->next;
    }
  }
  if(b != 0){
    float d = a/b;
    d = smallNumber(d);
    printf("solution: %.3f\n", d);
  } 
  else{
    printf("not solvable\n");
  }
}

void findSolutionDegreeTwo(List li){
  /* ax^2 + bx + c */
  float a=0, b=0, c=0;
  int positive=1;

  while( (li->tt != Symbol) || (li->t).symbol != '='){
    /* a, b or c */
    if( (li->tt)==Number ){
      if( !positive ){
        (li->t).number = - (li->t).number;
        positive = 1;
      }
      /* 5 x .... */
      if ( (li->next)!= NULL && (li->next)->tt == Identifier ){
        /* 5 x ^ (number) */
        if( (((li->next)->next)->next) != NULL && ((li->next)->next)->tt == Symbol && (((li->next)->next)->t).symbol == '^' && (((li->next)->next)->next)->tt == Number ){
            /* 5 x ^ 2 --> a */
          if( ((((li->next)->next)->next)->t).number == 2){
            a += (li->t).number;
            li = (((li->next)->next)->next)->next;
          }
            /* 5 x ^ 1 = 5 x --> b */
          else if(((((li->next)->next)->next)->t).number == 1 ){
            b += (li->t).number;
            li = (((li->next)->next)->next)->next;
            }
          else if(((((li->next)->next)->next)->t).number == 0 ){
            c += (li->t).number;
            li = (((li->next)->next)->next)->next;
            }
            /* 5 x ^ 0 = 5 --> c */
          else{
            c += (li->t).number;
            li = (((li->next)->next)->next)->next;
          } 
        }
        /* 5 x --> b */
        else{
          b += (li->t).number;
          li = ((li->next)->next);
        }
      }
      /* 5 --> c */
      else{
        c += (li->t).number;
        li = (li->next);
      }
    }
    else{ 
      /* + or - */
      if( (li->t).symbol == '-' ){
        positive = 0;
      } 
    li = li->next;
    }
  }

  li = li->next;
  positive = 1;

  /* after '=' */
  while( li != NULL ){
    /* a, b or c */
    if( (li->tt)==Number ){
      if( !positive ){
        (li->t).number = - (li->t).number;
        positive = 1;
      }
      /* 5 x .... */
      if ( (li->next)!= NULL && (li->next)->tt == Identifier ){
        /* 5 x ^ (number) */
        if(((li->next)->next) != NULL && ((li->next)->next)->tt == Symbol && (((li->next)->next)->t).symbol == '^' && (((li->next)->next)->next) != NULL && (((li->next)->next)->next)->tt == Number ){
            /* 5 x ^ 2 --> a */
          if(((((li->next)->next)->next)->t).number == 2){
            a -= (li->t).number;
            li = (((li->next)->next)->next)->next;
            }
            /* 5 x ^ 1 = 5 x --> b */
          else if( ((((li->next)->next)->next)->t).number == 1 ){
            b -= (li->t).number;
            li = (((li->next)->next)->next)->next;
            }
          else if( ((((li->next)->next)->next)->t).number == 0 ){
              c -= (li->t).number;
              li = (((li->next)->next)->next)->next;
            }
            /* 5 x ^ 0 = 5 --> c */
          else{
            c -= (li->t).number;
            li = (((li->next)->next)->next)->next;
            }
        }
        /* 5 x --> b */
        else{
          b -= (li->t).number;
          li = ((li->next)->next);
        }
      }
      /* 5 --> c */
      else{
        c -= (li->t).number;
        li = (li->next);
      }
    }
    else{ 
      /* + or - */
      if( (li->t).symbol == '-' ){
        positive = 0;
      } 
      li = li->next;
    }
  }

   if(a == 0 && b!= 0){
      c = rightPosition(c);
      float d = c/b;
      d = smallNumber(d);
      printf("solution: %.3f\n", d);
    }
    else if(a!=0 && b == 0 && c != 0){
      c = rightPosition(c);
      float d = c/a;
      if(d < 0){
        printf("not solvable\n");
      }
     else{
      d = sqrt(d);
      d = smallNumber(d);
      printf("solutions: %.3f %.3f\n", d, -d);
      }
    } 
    else if(a!=0 && b!=0 && c == 0){
      b = rightPosition(b);
      float d = b/a;
      if(b > 0){
        d= smallNumber(d);
        printf("solutions: %.3f 0.000\n", d);
      }
      else{
        d = smallNumber(d);
        printf("solutions: 0.000 %.3f\n", d);
      }
    }
    else if(((a != 0) && (b != 0) && (c != 0))){
    float d, x1, x2;
    d = (b * b) - (4 * a * c);
    if(d < 0){
      printf("not solvable\n");
    }
    else if(d == 0){
      x1 = (-b - sqrt(d) )/ (2*a);
      x1 = smallNumber(x1);
      printf("solution: %.3f\n", x1); 
    }
    else{
    x1 = (-b - sqrt(d) )/ (2*a);
    x2 = (-b + sqrt(d) )/ (2*a);
    x1 = smallNumber(x1);
    x2 = smallNumber(x2);
    if(x1 > x2){
      printf("solutions: %.3f %.3f\n", x1, x2); 
    }
    else{
      printf("solutions: %.3f %.3f\n", x2, x1); 
      }
    }
  }
  else{
    printf("not solvable\n");
  }
}


void recognizeExpressions() {
  char *ar;
  List tl, tl1;  
  int degree;
  ar = readInput();
  while (ar[0] != '!') {
    tl = tokenList(ar); 
    tl1 = tl;
    printf("give an equation: ");
    printList(tl);
    if(acceptEquation(&tl1) && tl1 == NULL) {
    tl1 = tl;
    if( isOneVariable(tl1) ){
      tl1 = tl;
      degree = findHighestDegree(tl1);
      printf("this is an equation in 1 variable of degree %d\n", degree); 
      tl1 = tl;
      if(degree == 1){
        findSolutionDegreeOne(tl1);
        }
      else if(degree == 2){
        findSolutionDegreeTwo(tl1);  
        }
      }
    else{
      printf("this is an equation, but not in 1 variable\n"); 
    }
    } else {
    printf("this is not an equation\n"); 
    }
    free(ar);
    freeTokenList(tl);
    printf("\n");
    ar = readInput();
  }
  free(ar);
  printf("give an equation: good bye\n");
}
