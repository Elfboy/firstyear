#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "queues.h"

int max(int a, int b){
  return (a > b ? a : b);
}

int min(int a, int b){
  return (a < b ? a : b);
}

/* The function action generates a new state from an existing state.
 */

State action(State s, int n, int cap1, int cap2) {
  switch (n) {
    case 0:
      break;
    case 1:
      s.sg1 = cap1 - s.sg1;
      break;
    case 2:
      s.sg2 = cap2 - s.sg2;
      break;
    case 3:
      s.sg1 = cap1 - s.sg1;
      s.sg2 = cap2 - s.sg2;
      break;
  }
  int remainder;
  remainder = min(cap1-s.sg1, cap2-s.sg2);
  if (remainder == 0) {
    remainder = max(cap1-s.sg1, cap2-s.sg2);
  }
  s.time += remainder;
  s.sg1 += remainder;
  s.sg2 += remainder;
  
  if (s.sg1 > cap1) {
    s.sg1 = cap1;
  }
  if (s.sg2 > cap2) {
    s.sg2 = cap2;
  }
  return s;
}

/* The function timeable checks whether a given time can be determined
 * exactly by two sandglasses with given capacities.
 */

int timeable(int cap1, int cap2, int goalTime) { 
  Queue q = newEmptyQueue();
  State s, temp;
  s.time = 0;
  s.sg1 = cap1;
  s.sg2 = cap2;
  enqueue(s, &q);
  
  if(goalTime%cap1 == 0){
    return 1;
  }
    if(goalTime%cap2 == 0){
    return 1;
  }
    if(goalTime%(cap1+cap2) == 0){
    return 1;
  }
  while (!isEmptyQueue(q)) {
    s = dequeue(&q);
    if (s.time == goalTime) {
      return 1;
    } else {
      for (int i = 0; i < 4; i++){
        temp = action(s, i, cap1, cap2);
        if(temp.time == goalTime){
          freeQueue(q);
          return 1;
        }
        if(((temp.time != s.time) || (temp.sg1 != s.sg1) || (temp.sg2 != s.sg2))  && (temp.time < goalTime)){
          insertUnique(temp, &q);
        }
      }
    }
  }
  freeQueue(q);
  return 0;
}

/* main performs a dialogue with the user. The user is asked to provide three numbers: 
 * the (positive) capacities of the sandglasses and the goal time (>= 0).
 * The program indicates whether the goal time can be determined exactly. 
 * The dialogue is ended by the input '0'.
 */

int main(int argc, char *argv[]){
  int cap1, cap2, goalTime; 
  printf("give the sandglass capacities and the goal time: ");
  scanf("%d",&cap1);
  while ( cap1 > 0 ) {
    scanf("%d",&cap2);
    assert( cap2 > 0 );
    scanf("%d",&goalTime);
    assert( goalTime >= 0 );
    if ( timeable(cap1,cap2,goalTime) ) {
      printf("%d and %d can time %d\n", cap1, cap2, goalTime);
    } else {
      printf("%d and %d cannot time %d\n", cap1, cap2, goalTime);
    }
    printf("\ngive the sandglass capacities and the goal time: ");
    scanf("%d",&cap1);
  }  
  printf("good bye\n");
  
  return 0;
}
