#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "queues.h"

List newEmptyList() {
  return NULL;
}

int isEmptyList (List li) {
  return ( li==NULL );
}

void listEmptyError() {    
  printf("list empty\n");
  abort();
}

List addItem(State s, List li) {
  List newList = malloc(sizeof(struct ListNode));
  assert(newList!=NULL);
  newList->item = s;
  newList->next = li;
  return newList;
}

// void printList(List li) {
//   while (li != NULL) {
//     printf("(sg1:%d, sg2:%d, time:%d)\n",li->item.sg1, li->item.sg2, li->item.time);

//     li = li->next;
//   }
//   printf("\n");
// }

List insert(State s, List li){
  List node = li;
  /*state is first one or somewhere in the beginning*/
  if ( isEmptyList(li) || (compare(s, li->item) == -1)){
    li = addItem(s, li);
    return li;
  }
   /*state is somewhere in the middle*/
  while (li->next != NULL){
    if ((compare(li->item, s) == -1) && (compare(li->next->item, s) == 1)){
      li->next = addItem(s, li->next); 
      return node;
    }
    li = li->next;
  }
  /*state is at the end*/
  if (compare(li->item, s) == -1){
    li-> next = addItem(s, NULL);
    return node;
  }

  return node;
}
 
State firstItem(List li) {
  if ( li == NULL ) {
    listEmptyError();
  }
  return li->item;
}

List removeFirstNode(List li) {
  List returnList;
  if ( li == NULL ) {
    listEmptyError();
  }
  returnList = li->next;
  free(li);
  return returnList;
}

void freeList(List li) {
  List li1;
  while ( li != NULL ) {
    li1 = li->next;
    free(li);
    li = li1;
  }
  return;
}

Queue newEmptyQueue () {
  Queue q;
  q.list = newEmptyList();
  q.lastNode = NULL;
  return q;
}

int isEmptyQueue (Queue q) {
  return isEmptyList(q.list);
}

void emptyQueueError () {
  printf("queue empty\n");
  exit(0);
}

int compare(State a, State b){
   /*a precedes b*/
  if ((a.time < b.time) || ((a.time == b.time) && ((a.sg1 < b.sg1) || ((a.sg1 == b.sg1) && (a.sg2 < b.sg2))))) {
    return -1;
  }
  /*duplicate*/
  if ((a.time == b.time) && (a.sg1 == b.sg1) && (a.sg2 == b.sg2)){
    return 0;
  }
    /*b precedes a*/
  return 1;
}
   
void enqueue (State s, Queue *qp) {
  if ( isEmptyList(qp->list) ) {
    qp->list = addItem(s,NULL);
    qp->lastNode = qp->list;
  } else {
    (qp->lastNode)->next = addItem(s,NULL);
    qp->lastNode = (qp->lastNode->next);
  }
}

void insertUnique (State s, Queue *qp) {
  qp->list = insert(s, qp->list);
}

State dequeue(Queue *qp) {
  State s = firstItem(qp->list);
  qp->list = removeFirstNode(qp->list);
  if ( isEmptyList(qp->list) )  {
    qp->lastNode = NULL;
  }
  return s;
}

void freeQueue (Queue q) {
	freeList(q.list);
}
